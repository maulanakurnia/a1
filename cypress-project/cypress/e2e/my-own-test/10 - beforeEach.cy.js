/// <reference types="cypress" />

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im11ZnJhZG1hYm5pIiwiX2lkIjoiNjM5ODFmNGI2NWI2OTYwMDA4ODI4ZTk1IiwibmFtZSI6Ik1hdWxhbmEgS3VybmlhIiwiaWF0IjoxNjcwOTEzODcyLCJleHAiOjE2NzYwOTc4NzJ9.TW6kBF1v3FHYempXB9mzJeHlG6e3R0qJQhJAUGuQ1xE"

describe('Logging In With Cypress', ()=>{
    before(()=> {
        cy.then(() => {
            window.localStorage.setItem('__auth__token', token)
        })
    })

    beforeEach(()=> {
        cy.viewport('macbook-13')
        cy.visit('https://codedamn.com/')
    })

    it("Authenticated", ()=>{
        cy.get('Sign In').should('not.exist')
    })
    
})
