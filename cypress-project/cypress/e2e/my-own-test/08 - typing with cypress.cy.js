/// <reference types="cypress" />

describe('Basic Test', ()=>{
    it('Typing with cypress', ()=>{
        cy.viewport('macbook-13')
        cy.visit('https://codedamn.com/')
        
        // Sign in page
        cy.contains('Sign In').click()
        cy.wait(5000)

        // get type with cypress
        cy.get('[data-testid=username]').type('robot', {force: true})
        cy.get('[data-testid=password]').type('robot', {force: true})
    })
})