/// <reference types="cypress" />

describe('Logging In With Cypress', ()=>{
    it("Login should display correct error", ()=>{
        cy.viewport('macbook-13')
        cy.visit('https://codedamn.com/')
        
        // Sign in page
        cy.contains('Sign In').click()
        cy.wait(5000)

        // get type with cypress
        cy.get('[data-testid=username]').type('robot', {force: true})
        cy.get('[data-testid=password]').type('robot', {force: true})
        
        cy.get('[data-testid=login]').click({force: true})
        
        cy.wait(5000)
        cy.contains('Unable to authorize. Please check username/password combination').should('exist')
    })

    it("Login should work fine", ()=>{
        cy.viewport('macbook-13')
        cy.visit('https://codedamn.com/')
        
        // Sign in page
        cy.contains('Sign In').click()
        cy.wait(5000)

        // get type with cypress
        cy.get('[data-testid=username]').type('mufradmabni', {force: true})
        cy.get('[data-testid=password]').type('mysecretpass12!#', {force: true})
        
        cy.get('[data-testid=login]').click({force: true})
        
        cy.url().should('include', '/dashboard')
    })
})
