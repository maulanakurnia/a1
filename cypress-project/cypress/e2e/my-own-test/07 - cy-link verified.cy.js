/// <reference types="cypress" />

describe('Basic Test', ()=>{
    it('Login Page redirect link is work', ()=>{
        cy.viewport('macbook-13')
        cy.visit('https://codedamn.com/')
        
        // click element contain 'Sign In' text
        cy.contains('Sign In').click()
        
        // click element contain 'Create one' text
        cy.wait(3000)
        cy.contains('Create Account').click({force: true})
        cy.url().should('eq', 'https://codedamn.com/register')
        
        cy.url().then(value => cy.log('The current URL is ', value))

        // go back to sign in page
        cy.go('back')

        // click element contain 'Forgot your password?' text
        cy.contains('Forgot your password?').click({force: true})
        cy.url().should('include', '/password-reset')
    })

})
